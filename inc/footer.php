<!--<section class="top__foot">-->
<!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">-->
<!--    <div class="block_policy2">-->
<!--      <ul>-->
<!--        <li class="item_1">-->
<!--          <div class="item_inner">-->
<!--            <div class="icon icon1"></div>-->
<!--            <div class="content">-->
<!--              <a href="#">free delivery</a>-->
<!--              <p>From $59.89</p>-->
<!--            </div>-->
<!--          </div>-->
<!--        </li>-->
<!--        <li class="item_2">-->
<!--          <div class="item_inner">-->
<!--            <div class="icon icon2"></div>-->
<!--            <div class="content">-->
<!--              <a href="#">support 24/7</a>-->
<!--              <p>Online 24 hours</p>-->
<!--            </div>-->
<!--          </div>-->
<!--        </li>-->
<!--        <li class="item_3">-->
<!--          <div class="item_inner">-->
<!--            <div class="icon icon3"></div>-->
<!--            <div class="content">-->
<!--              <a href="#">free return</a>-->
<!--              <p>365 a day</p>-->
<!--            </div>-->
<!--          </div>-->
<!--        </li>-->
<!--        <li class="item_4">-->
<!--          <div class="item_inner">-->
<!--            <div class="icon icon4"></div>-->
<!--            <div class="content">-->
<!--              <a href="#">payment method</a>-->
<!--              <p>secure payment</p>-->
<!--            </div>-->
<!--          </div>-->
<!--        </li>-->
<!--        <li class="item_5">-->
<!--          <div class="item_inner">-->
<!--            <div class="icon icon5"></div>-->
<!--            <div class="content">-->
<!--              <a href="#">big saving</a>-->
<!--              <p>weekend sales</p>-->
<!--            </div>-->
<!--          </div>-->
<!--        </li>-->
<!--      </ul>-->

<!--    </div>-->

<!--  </div>-->
<!--</section>-->



<section class="foot__bootom">
  <div class="block__policy_6">
    <div class="f__bootom_left">
      <div class="copy__right">
          <p>© copyroght 2020 Bae System</p>     
      </div>
    <div>
  </div>
</section>


<script src="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>
<script>
  AOS.init({
    duration: 2500,
  })
</script>

<script>
   new Glider(document.querySelector('.glider'), {
      slidesToShow: 6,
      dots: '#dots',
      draggable: true,
      arrows: {
        prev: '.glider-prev',
        next: '.glider-next'
      }
    });
</script>
<script>
  new Glider(document.querySelector('.product_glider2'), {
     slidesToShow: 6,
     dots: '#dots',
     draggable: true,
     arrows: {
       prev: '.glider-prev',
       next: '.glider-next'
     }
   });
</script>


<script src="assets/js/main.js"></script>
</body>

</html>