<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> </title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="assets/vendors/jquery.min.js"></script>
  <link rel="shortcut icon" href="assets/img/black-logo.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/custom.css">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/media.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <link rel="stylesheet" href="assets/css/docs.theme.min.css">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css">
  <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.css">

<body>

 <section class="headers">
  <div class="top_headers">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="header_notice">
            <p>CLICK & COLLECT NOW AVAILABLE IN KHOBAR, DAMMAM & RIYADH</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mid_headers">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
           <div class="header_serach">
             <button class="btn_header_search">
               <span class="flaticon-search"></span>
             </button>
             <input type="search" placeholder="Search for products, brands and more">  
           </div>
        </div>
        <div class="col-md-4 col-6">
          <div class="header_logo">
                <a href="index.php"><img src="assets/img/logo2.png" style="width: 150px;"></a>
          </div>
        </div>
        <div class="col-md-4 col-6">
          <ul class="right_side">
                <!--<li><a href="#">Find a Store</a></li>-->
                <!--<li><a href="#">Account <span class="flaticon-down-chevron" id="chervon_icon"></span></a></li>-->
                <li><a href="cart.php"><span id="cart_number">1</span><span class="flaticon-shopping-cart" id="shopping_cart"></span></a>
                  <span class="cart_text">Cart</span>
                </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="main_menu">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="main_menu_list">
            <li><a href="#">Hot Deals</a></li>
            <li><a href="#">Fresh</a></li>
            <li><a href="#">Food & Beverages</a></li>
            <li><a href="#">Healthy living</a></li>
            <li><a href="#">Healthy & Beauty</a></li>
            <li><a href="#">Household</a></li>
            <li><a href="#">Baby</a></li>
            <li><a href="#">Pets</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
 </section>