<?php include('inc/header.php') ?>
<section class="ekart__hero">
	<div class="ekart__hero_wrap">
		<div class="ekart__hero_left">
			<div class="main_slider">
				<div class="owl-carousel owl-theme slider_carousel">
					<div class="item">
						<div class="slider_img">
							<img src="assets/img/slider.png" alt="">
						</div>
					</div>
					<div class="item">
						<div class="slider_img">
							<img src="assets/img/slider2.png" alt="">
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="ekart__hero_right">
			<ul>
				<li><a href="#"><img src="assets/img/left_slide_img.jpg" alt=""></a></li>
				<li><a href="#"><img src="assets/img/left_slide_img2.jpg" alt=""></a></li>
			</ul>
		</div>
	</div>
</section>

<!--<section class="h__collection">-->
<!--	<div class="h_collection_title">-->
<!--		<h4>Shop by Department</h4>-->
<!--	</div>-->
<!--	<div class="cus__conatiner">-->
<!--	  <div class="h__collection_row">-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Fruits & Vegatable</a></h5>-->
<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/vegitable.png" alt="Vegatable"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Bakery</a></h5>-->
<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/bakery.png" alt="Bakery"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Fresh Meat</a></h5>-->

<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/meat.png" alt="Interior & Furniture"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Chicken</a></h5>-->

<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/chicken.png" alt="Mobile & Tablets"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Sea Foods</a></h5>-->
<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/sea_food.png" alt="Sunglasses"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--	  </div>-->
<!--	  <div class="h__collection_row">-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Dairy</a></h5>-->
<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/dairy.png" alt="Dairy"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Food & Beverages</a></h5>-->
<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/food.png" alt="food & beverages"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">breakfast</a></h5>-->

<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/breakfast.png" alt="breakfast"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">Snacks & Confectionery</a></h5>-->
<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/snack'.png" alt="snacks & confectionery"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--		  <div class="h__collection_content">-->
<!--			<div class="h__collection_caption">-->
<!--			  <h5><a href="#">oil & ghee</a></h5>-->
<!--			</div>-->
<!--			<div class="h__collection_img">-->
<!--			  <a href="#"><img src="assets/img/oil.png" alt="Oil & Ghee"></a>-->
<!--			</div>-->
<!--		  </div>-->
<!--		</div>-->
<!--		<div class="h__collection_col">-->
<!--			<div class="h__collection_content">-->
<!--			  <div class="h__collection_caption">-->
<!--				<h5><a href="#">Rice & Pasta</a></h5>-->
<!--			  </div>-->
<!--			  <div class="h__collection_img">-->
<!--				<a href="#"><img src="assets/img/TM2.5RICE&PASTA-20190612-033910.png" alt="rice & pasta"></a>-->
<!--			  </div>-->
<!--			</div>-->
<!--		  </div>-->
<!--		  <div class="h__collection_col">-->
<!--			<div class="h__collection_content">-->
<!--			  <div class="h__collection_caption">-->
<!--				<h5><a href="#">Water & Beverages</a></h5>-->
<!--			  </div>-->
<!--			  <div class="h__collection_img">-->
<!--				<a href="#"><img src="assets/img/TM2.9WATER&BEVERAGES-20190612-033824.png" alt="water & beverages"></a>-->
<!--			  </div>-->
<!--			</div>-->
<!--		  </div>-->
<!--		  <div class="h__collection_col">-->
<!--			<div class="h__collection_content">-->
<!--			  <div class="h__collection_caption">-->
<!--				<h5><a href="#">Disposables</a></h5>-->
<!--			  </div>-->
<!--			  <div class="h__collection_img">-->
<!--				<a href="#"><img src="assets/img/disposel.png" alt="disposables"></a>-->
<!--			  </div>-->
<!--			</div>-->
<!--		  </div>-->
<!--		  <div class="h__collection_col">-->
<!--			<div class="h__collection_content">-->
<!--			  <div class="h__collection_caption">-->
<!--				<h5><a href="#">Baby</a></h5>-->
<!--			  </div>-->
<!--			  <div class="h__collection_img">-->
<!--				<a href="#"><img src="assets/img/baby.png" alt="baby"></a>-->
<!--			  </div>-->
<!--			</div>-->
<!--		  </div>-->
<!--		  <div class="h__collection_col">-->
<!--			<div class="h__collection_content">-->
<!--			  <div class="h__collection_caption">-->
<!--				<h5><a href="#">healthy living</a></h5>-->
<!--			  </div>-->
<!--			  <div class="h__collection_img">-->
<!--				<a href="#"><img src="assets/img/healthy.png" alt="Oil & Ghee"></a>-->
<!--			  </div>-->
<!--			</div>-->
<!--		  </div>-->
<!--	  </div>-->
<!--	</div>-->
<!--  </section>-->

<section class="ekart__product_sec01">
	<div class="cus__conatiner">
		<div class="h_collection_title">
			<h4>Shop Now</h4>
		</div>
		<div class="">
			<div class="">
				<div class="grid_sec">
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/productsa.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Antiseptic Disinfectant Liquid - For First Aid, Surface Cleaning, & Personal Hygiene</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products2.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Disinfectant Surface Cleaner - Citrus</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products3.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Pure & Gentle Bathing Bar</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products5.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Charge Effervescent Tablets with Natural Vitamin C and Zinc - Orange Flavour</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products6.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Kiwi</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products7.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Masterblaster Germ Protection Liquid Hand Wash Refill</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products8.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Cream Beauty Bathing Bar</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products9.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Bathing Soap - Ayurvedic Soap with 18 Herbs</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
					<div class="ekart__product_box">
						<div class="ekart__product_box_image">
							<a href="#"><img src="assets/img/products5.jpg" alt="detol"></a>
						</div>
						<div class="ekart__product_caption">
							<div class="product_name">Intex it-pb11k 11800 -mAh Li-Ion Power Bank Grey</div>
						</div>
						<div class="ekart__product_price">
							<span class="discount_price stroke">Rs 2,300</span>
							<span class="mrp">Rs 749</span>
							<span class="product_disc"> 67% OFF </span>
						</div>
						<a href="#">
							<div class="add_cart_button">
								<div class="add_cart">Add To Cart</div>
								<div class="add_plus"><i class="fas fa-plus"></i></div>
							</div>
						</a>
					</div>
				</div>

				<button role="button" aria-label="Previous" class="glider-prev"><span class="ti-angle-left" id="ekart__slider_btn"></span></button>
				<button role="button" aria-label="Next" class="glider-next"><span class="ti-angle-right" id="ekart__slider_btn"></span></button>
				<div role="tablist" class="dots"></div>
			</div>
		</div>
	</div>
</section>



<?php include('inc/footer.php') ?>