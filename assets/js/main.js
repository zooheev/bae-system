
 $(document).ready(function (){
  $('.slider_carousel').owlCarousel({
    loop: true,
    margin:0,
    nav:false,
    dotsEach: true,
    dotData: false,
    responsiveClass: true,
    autoplay:true,
    slideSpeed : 3000,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',    
    owldots:false,
    responsive: {
      0: {    
        items: 1,
        nav: true,
        dots:false
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
      }
    }
  });

});


$('.start_number').each(function () {
  var size = $(this).text().split(".")[1] ? $(this).text().split(".")[1].length : 0;
  $(this).prop('Counter', 0).animate({
    Counter: $(this).text()
  }, {
    duration: 5000,
    step: function (func) {
      $(this).text(parseFloat(func).toFixed(size));
    }
  });
});




  $(document).ready(function (){
    $('.serivice_slider').owlCarousel({
      loop: true,
      margin:0,
      nav:true,
      dotsEach: false,
      dotData: false,
      responsiveClass: true,
      autoplay:false,
      slideSpeed : 3000,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      owldots:false,
      navText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 3,
          
        }
      }
  });
    });

    $(document).ready(function () {
      $(window).scroll(function() {
        var sticky = $('.headers'),
          scroll = $(window).scrollTop();
         
        if (scroll  >=40) { 
          sticky.addClass('fixeds'); }
        else { 
         sticky.removeClass('fixeds');
      
      }
      });
    })


    $(document).ready(function () {
      $('.mobo_button').click(function(){
       $(this).toggleClass('cross_bar')
       $('.mobo__menu_link').toggleClass('active_menu');    
      }); 
   });




   
jQuery(document).ready(function($) {
  $('.slider').slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    responsive: [{
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
       breakpoint: 400,
       settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
       }
    }]
});
});

/**
 * jquery.owl-filter.js
 * Create: 07-09-2016
 * Author: Bearsthemes
 * Version: 1.0.0
 */
