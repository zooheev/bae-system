<?php include('inc/header.php') ?>
<div class="container">
    <div class="breadcrumb">
        <ul class="breadcrumbs-list">
            <li class="breadcrumbs-item"><a href="#">Home</a></li>
            <li class="breadcrumbs-item"><a href="#">Checkout</a></li>
            <li class="breadcrumbs-product"><a href="#">Cart</a></li>
        </ul>
    </div>
    <div class="cart_page">
        <div class="cart_left">
            <div class="wishlist_2">
                <div class="pr_l">

                    <img src="assets/img/p1.webp">

                </div>

                <div class="pr_l">

                    <h3>Dimyra Men's White Black Plain Cotton Round Neck Casual T-Shirt NR</h3>
                    <p><span>Size Men's Upper : M</span><span></span></p>


                    <div class="buy">
                        <a href="#" class="">Remove from Cart </a>
                    </div>

                </div>
                <!-- <div class="pr_l">
    
                    <div class="block_sec">  
                        <select name="" id="">
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                        </select>
                    </div>
                  
    
                    <div class="view_p">
                        <a href="#" class="remove_underline">Update Card</a>
                    </div>
                </div> -->
                <div class="pr_l">

                </div>
            </div>
            <div class="wishlist_2">
                <div class="pr_l">

                    <img src="assets/img/p1.webp">

                </div>

                <div class="pr_l">

                    <h3>Dimyra Men's White Black Plain Cotton Round Neck Casual T-Shirt NR</h3>
                    <p><span>Size Men's Upper : M</span><span></span></p>


                    <div class="buy">
                        <a href="#" class="">Remove from Cart </a>
                    </div>

                </div>
                <!-- <div class="pr_l">
    
                    <div class="block_sec">  
                        <select name="" id="">
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">7</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                        </select>
                    </div>
                  
    
                    <div class="view_p">
                        <a href="#" class="remove_underline">Update Card</a>
                    </div>
                </div> -->
                <div class="pr_l">

                </div>
            </div>
        </div>
        <div class="cart_left">
            <div class="account_detail">
                <h4>Order Summary</h4>
                <ul>
                    <li>Total MRP (Inclusive of all taxes)<span>$1498</span></li>
                    <li>Shipping Charges<span>Free</span></li>
                    <li>Payable Amount<span>$1498</span></li>
                    <li>Final Amount<span>$1498</span></li>
                </ul>
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#query">Generate Enquiries</a>
            </div>
        </div>
    </div>
</div>


<!-- The Modal -->
<div class="modal fade" id="query">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">



            <!-- Modal body -->
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal_heading">
                    <h4>#StayHomeStaySafe</h4>
                    <p>We Urge you to stay home and stay safe</p>
                </div>
              <form action="">
                <div class="modal_body">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="modal_input">
                                <label for="">Full Name</label>
                                <input type="text" name="" id="" placeholder="Full Name" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal_input">
                                <label for="">Mobile Number</label>
                                <input type="text" name="" id="" placeholder="Mobile number" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal_input">
                                <label for="">Email</label>
                                <input type="email" name="" id="" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal_input">
                                <label for="">State</label>
                                <input type="text" name="" id="" placeholder="State" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal_input">
                                <label for="">City</label>
                                <input type="text" name="" id="" placeholder="City" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal_input">
                                <label for="">Zip</label>
                                <input type="text" name="" id="" placeholder="Zip" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="modal_input">
                                <label for="">Notes</label>
                                <textarea name="" id=""  placeholder="Notes"></textarea>           
                             </div>
                        </div>
                        <div class="col-md-12">
                            <div class="accept">
                                <input type="submit" value="Submit">
                            </div>
                        </div>
                     
                    </div>
                </div>
              </form>
            </div>

            <!-- Modal footer -->
            <!-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div> -->

        </div>
    </div>
</div>
<?php include('inc/footer.php')?>